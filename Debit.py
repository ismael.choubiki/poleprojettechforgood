import cv2
import numpy as np
import tkinter as tk

def calculer_debit_beton(video_path,hvalue,wvalue):

    cap = cv2.VideoCapture(video_path)
    
    # Paramètres du flot optique
    params = {
        'pyr_scale': 0.5,
        'levels': 3,
        'winsize': 15,
        'iterations': 3,
        'poly_n': 5,
        'poly_sigma': 1.2,
        'flags': 0
    }
    
    # Initialiser le premier cadre
    _, frame_prev = cap.read()
    frame_prev_gray = cv2.cvtColor(frame_prev, cv2.COLOR_BGR2GRAY)
    
    total_flow = 0
    frame_count = 0
    
    while True:
        _, frame = cap.read()
        
        if frame is None:
            break
        
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        dim1=frame.shape[0]
        dim2=frame.shape[1]
        c=(dim2*dim1)/(wvalue*hvalue)

        # Calculer le flot optique dense
        flow = cv2.calcOpticalFlowFarneback(frame_prev_gray, frame_gray, None, **params)

        # Calculer le débit du béton
        flow_x = np.mean(flow[:, :, 0])
        flow_y = np.mean(flow[:, :, 1])
        debit_instantane = np.sqrt(flow_x**2 + flow_y**2)
        total_flow += debit_instantane *hvalue*wvalue/c
        
        # Afficher la vidéo en direct
        cv2.imshow('Video', frame)
        cv2.putText(frame, f'Débit instantané: {debit_instantane:.2f}', (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        print(f'Débit instantané en m^3/s: {debit_instantane*hvalue*wvalue/c:.2f}')
        cv2.imshow('Video', frame)
        
        frame_prev_gray = frame_gray
        frame_count += 1
        
        if cv2.waitKey(1) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()
    
    # Estimer le débit moyen
    debit_moyen = total_flow / frame_count
    
    print(debit_moyen)
    return debit_moyen

# Exemple d'utilisation
video_path = 0
window=tk.Tk()

l1=tk.Label(window,text='saisir la largeur de la gouttière (en m)')
l1.pack()
w=tk.Entry(window)
w.pack()

l2=tk.Label(window,text='saisir la hauteur de la gouttière (en m)')
l2.pack()
h=tk.Entry(window)
h.pack()

b=tk.Button(window,text='Lancer',command=lambda: calculer_debit_beton(video_path,float(h.get()),float(w.get())))
b.pack()


window.mainloop()