import cv2
import numpy as np


def detect_particles(image):
    # Appliquer un seuillage adaptatif pour binariser l'image
    _, binary_image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    # Effectuer une opération de morphologie pour supprimer le bruit et fermer les trous
    kernel = np.ones((5, 5), np.uint8)
    binary_image = cv2.morphologyEx(binary_image, cv2.MORPH_CLOSE, kernel)

    # Détecter les contours des particules
    contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Extraire les positions des centres de masse des particules
    positions = []
    for contour in contours:
        M = cv2.moments(contour)
        if M["m00"] != 0:
            cx = int(M["m10"] / M["m00"])
            cy = int(M["m01"] / M["m00"])
            positions.append((cx, cy))

    return positions


def calculate_flow_velocity(positions, time_interval):
    # Vérifier s'il y a suffisamment de positions pour calculer la vitesse
    if len(positions) < 2:
        return 0.0  # ou une valeur par défaut appropriée

    # Calculer la distance parcourue par la particule
    distance = np.linalg.norm(np.diff(positions, axis=0))

    # Vérifier si la distance est inférieure à un seuil
    distance_threshold = 1000 # Définir le seuil de distance approprié
    if distance < distance_threshold:
        return 0.0  # ou une valeur par défaut appropriée

    # Calculer la vitesse d'écoulement en mètres par seconde (ou autre unité appropriée)
    flow_velocity = distance / time_interval

    return flow_velocity


# Paramètres
video_source = "video3.mp4"  # Source vidéo (0 pour la webcam)
time_interval = 1.0  # Intervalle de temps entre les images consécutives en secondes

# Capture vidéo
cap = cv2.VideoCapture(video_source)

# Variables pour le suivi des particules
prev_frame = None
prev_positions = []

while True:
    # Capture de l'image
    ret, frame = cap.read()
    if not ret:
        break

    # Conversion en niveaux de gris
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Prétraitement de l'image (si nécessaire)
    # Appliquez des opérations de filtrage, de seuillage, etc.

    # Détection des particules
    positions = detect_particles(gray)

    # Suivi des particules
    if prev_frame is not None and len(prev_positions) > 0:
        # Calculez la vitesse d'écoulement
        flow_velocity = calculate_flow_velocity(positions, time_interval)
        print("Vitesse d'écoulement :", flow_velocity, "m/s")

        # Affichage des résultats sur l'image
        # Par exemple, vous pouvez dessiner des cercles ou des vecteurs pour indiquer la vitesse des particules

    # Mise à jour des images et des positions précédentes
    prev_frame = gray.copy()
    prev_positions = positions.copy()

    # Affichage de l'image
    cv2.imshow('Video', frame)

    # Sortie de la boucle si la touche 'q' est pressée
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break